import React from 'react';
import PropTypes from 'prop-types';

function Clima({resultado}) {
    const {main, name}= resultado;
    if(!name){
        return null;
    }
    const  gradosKelvin = 273.15;
    return (
        <div className="card-panel white col s12">
            <div className="black-text">
                <p >el clima en {name} es de</p>
                <p className="temperatura">{parseFloat(main.temp - gradosKelvin, 10).toFixed(2)}<span>&#x2103;</span></p>
                <p >Temperatura maxima {parseFloat(main.temp_max - gradosKelvin, 10).toFixed(2)}<span>&#x2103;</span></p>
                <p >Temperatura minima {parseFloat(main.temp_min - gradosKelvin, 10).toFixed(2)}<span>&#x2103;</span></p>

            </div>
        </div>
    );

}
Clima.propTypes={
    resultado: PropTypes.object.isRequired
}
export default Clima;