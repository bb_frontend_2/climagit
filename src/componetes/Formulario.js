import React, {useEstate, useState} from 'react';
import PropTypes from 'prop-types';

function Formulario({busqueda, guardarBusqueda, guardarConsultar}) {
    //stat del formulario//

    const [error, guardarError]= useState(false);

    const handlechange = (e)=>{
        e.preventDefault();
        //actualizar el state
        guardarBusqueda({
            ...busqueda,
            [e.target.name]: e.target.value,

        })


    }
   const handleSubmit=(e)=>{
    e.preventDefault();
     if(ciudad.trim() == ''|| pais.trim() == ''){
        guardarError(true);
        return;
     }else{
         guardarError(false);

     }
     guardarConsultar(true);

   }
    //extraer  cidudad y pais de  el objeto
    const {ciudad, pais}= busqueda;

    return (
       <form
        onSubmit={handleSubmit}
       >
           {(error)?(<p className="red darken-4  error">Todos Los Campos Son Obligatorios</p>):(null)}
           <div className="input-field col  s12">
            <input
                type="text"
                name="ciudad"
                id= "ciudad"
                value={ciudad}
                onChange ={handlechange}
            />
            <label htmlFor="ciudad">Ciudad:</label>

           </div>
           <div className="input-field  col s12">
                <select
                    name= "pais"
                    id="pais"
                    value={pais}
                    onChange ={handlechange}
                >
                    <option>--Seleccione Un Pais--</option>
                    <option value="US">Estados Unidos</option>
                    <option value="MX">México</option>
                    <option value="AR">Argentina</option>
                    <option value="CO">Colombia</option>
                    <option value="CR">Costa Rica</option>
                    <option value="ES">España</option>
                    <option value="PE">Perú</option>

                </select>
                <label htmlFor="pais">Pais:</label>

           </div>
           <div className="input-field col s12">
                <button
                    type="submit"
                    className="waves-effect waves-light btn-large btn-block yellow accent-4 col s12"
                >Buscar Clima</button>
            </div>
       </form>
    );
}
Formulario.propTypes = {
    busqueda: PropTypes.object.isRequired,
    guardarBusqueda: PropTypes.func.isRequired,
    guardarConsultar: PropTypes.func.isRequired,


}
export default Formulario;