import React,{useState, useEffect, Fragment } from "react";
import './App.css';
import styled from '@emotion/styled'
import Header from "./componetes/Header";
import Formulario from "./componetes/Formulario";
import Clima from "./componetes/Clima";
function App() {

  const [busqueda, guardarBusqueda]= useState({
    ciudad: '',
    pais: ''
  });
  const [consultar, guardarConsultar]= useState(false);
  const [resultado, guardarResultado]= useState({})
  const [error, guardarError]= useState(false);
  const {ciudad, pais} = busqueda;

useEffect(()=>{
  const consultarApi = async()=>{
    if(consultar){
      const appId ='282ebf3755d35a3d7e6cd2b722239309';
      const url = `http://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`
      const  respuesta =  await fetch(url);
      const resultado = await respuesta.json();
      guardarResultado(resultado);
      guardarConsultar(false);

      //detecta si  hubo resultados correctos
      if(resultado.cod === '404'){
          guardarError(true);
      }else{
          guardarError(false);
      }

    }
  }
    consultarApi();
}, [consultar]);

  return (
    <div className="App">
      <Header
        titulo="clima con react"
      />
      <div className="contenedor-form">
        <div className="container">
          <div className="row">
              <div className="col m6 s12">
                  <Formulario
                  busqueda = {busqueda}
                  guardarBusqueda = {guardarBusqueda}
                  guardarConsultar= {guardarConsultar}
                  />
              </div>
              <div className="col m6 s12">
                {
                  (error== true)?
                  (<p className="red darken-4  error">no se encontro esta ciudad</p>):
                  (
                    <Clima
                      resultado={resultado}
                    />
                  )
                }

              </div>
          </div>
        </div>

      </div>

    </div>
  );
}

export default App;
